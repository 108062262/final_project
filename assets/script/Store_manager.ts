// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Store_manager extends cc.Component {

    // LIFE-CYCLE CALLBACKS:

    @property({type:cc.Label})
    player_fruit_label : cc.Label = null;

    @property({type:cc.Node})
    items : cc.Node[] = [];

    private total_manager:any = null;

    onLoad () {
        this.total_manager = cc.find("Total_manager").getComponent("Total_manager");
        
    }

    start () {
        this.player_fruit_label.string =  "fruits : " + String(this.total_manager.fruit);
        this.init_items();
    }

    back_to_menu(){
        document.body.style.cursor = "auto";
        cc.director.loadScene("menu");

    }
    init_items(){
        for(let i = 0 ; i < this.items.length ; i++){
            var Store_item_script = this.items[i].getChildByName("Store_item_script").getComponent("Store_item_script");
            
            Store_item_script.set_skin_number(i + 1);

            if(this.total_manager.current_skin == i + 1){
                Store_item_script.ischoosed = true;
            }


            if(Store_item_script.isbought(i+1)){
                Store_item_script.issold = true;
            }
        }
    }




    

    set_fruit(){
        var data = cc.find("fruit_set(debug)").getComponent(cc.EditBox).string;
        this.total_manager.update_to_firebase("fruit", data);
        alert("set success! plz logout and login");
    }

    clear_skin(){
        this.total_manager.update_to_firebase("skin", 1);
        this.total_manager.update_to_firebase("current_skin", 1);
        alert("clear success! plz logout and login");
    }

    // update (dt) {}
}
