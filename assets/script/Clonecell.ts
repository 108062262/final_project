import { threadId } from "node:worker_threads";
import Player from "./Player";

const {ccclass, property} = cc._decorator;


@ccclass
export default class Clonecell extends cc.Component {
    private player = null;
    public canGather : boolean = false;
    public anim = null;
    private bodyAnim = null;

    onLoad () {
        this.anim = this.node.getChildByName("mini face").getComponent(cc.Animation);
        this.bodyAnim = this.node.getComponent(cc.Animation);
        
    }

    start () {
        this.player = cc.find("player");    
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        //this.schedule(function() {
        //    this.getComponent(cc.RigidBody).linearVelocity = this.player.getComponent(cc.RigidBody).linearVelocity;
        //}, 0.1);
        
    }

    onBeginContact( contact, self, other ){
        if(other.tag == 5){
            console.log("clonecell touch enemy!");
            this.player.getComponent(Player).isHurt = true;
            this.player.getComponent(Player).eatenNum --;
            this.player.getComponent(Player).scatterNum --;
            console.log(this.player.getComponent(Player).eatenNum);
            //this.player.anim.play("hurtFace");

            if(!this.bodyAnim.getAnimationState("die").isPlaying){
                this.anim.play("dieFace");
                this.bodyAnim.play("die");
            }
            

            // this.scheduleOnce(this.dead(), 0.8);

            this.bodyAnim.on('finished',  this.dead, this);

            // this.node.destroy();

            
            // update UI cell label
            this.player.getComponent(Player).game_manager.update_UI_cell_lable(this.player.getComponent(Player).eatenNum);
        }
    }

    dead(){
        cc.log("cell dead");
        this.node.destroy();
    }

    update(){
        if(this.player.getComponent(Player).cell_canJump == true){
            this.getComponent(cc.RigidBody).applyForceToCenter(cc.v2(- 100  * cc.director.getPhysicsManager().gravity.x,
                                                         - 100 * cc.director.getPhysicsManager().gravity.y),true);
        }
    }



    
}
