import { threadId } from "node:worker_threads";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Flower extends cc.Component {

    @property(cc.Prefab)
    flower_bullet : cc.Prefab = null;

    @property
    shootDistance: number = 180;

    @property
    shootFrequency: number = 4;

    private anim = null;
    private animState_attack = null;
    private animState_idle = null;
    private isShooting : boolean = false;
    private idle: boolean = false;


    onLoad () {
        this.anim = this.node.getComponent(cc.Animation);
    }

    start () {
        this.anim.play("flower_idle");
        this.schedule(function(){
            this.shoot();
        }, this.shootFrequency);
    }

    update(){
        if(this.isShooting && this.anim.getAnimationState('flower_idle').isPlaying){
            this.animState_idle = this.anim.stop("flower_idle");
        }else if(!this.isShooting && !this.anim.getAnimationState('flower_idle').isPlaying){
            this.animState_idle = this.anim.play("flower_idle");
        }
    }
    shoot(){
        this.isShooting = true;
        this.anim.play("flower_attack");
        var flower_bullet;
        this.scheduleOnce(function() {
        flower_bullet = cc.instantiate(this.flower_bullet);
        flower_bullet.parent = this.node;
        flower_bullet.setPosition(0,0);
        let action;
        action = cc.spawn(cc.sequence(cc.moveBy(0.5, -this.shootDistance, 0), cc.delayTime(0.2), cc.removeSelf()),  cc.fadeTo(0.5, 0));
        flower_bullet.runAction(action);
        this.isShooting = false;
        }, 0.8);
        

        
    }


    
}
