const {ccclass, property} = cc._decorator;

@ccclass
export default class HiddenArea extends cc.Component {



    onload () {
        cc.director.getPhysicsManager().enabled = true;
    }

    onBeginContact(contact, self, other){
        if(other.node.name == "player"){
            let action = cc.fadeOut(1);
            this.node.runAction(action);
            this.scheduleOnce(function(){
                this.node.destroy();
            }, 1);
        }
    }
}
