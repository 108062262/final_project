// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Login_manager extends cc.Component {


    @property({type:cc.EditBox})
    private email : cc.EditBox = null;

    @property({type:cc.EditBox})
    private password : cc.EditBox = null;

    @property({type:cc.AudioClip})
    se_click: cc.AudioClip = null; 

    private total_manager:any = null;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.total_manager = cc.find("Total_manager").getComponent("Total_manager");
    }

    login(message){
        cc.audioEngine.playEffect(this.se_click, false);
        var auth = firebase.auth();
        let log_in_email_txt = this.email.string;
        let log_in_password_txt = this.password.string;

        var account = this.email_to_account(log_in_email_txt);

        const promise = auth.signInWithEmailAndPassword(log_in_email_txt,log_in_password_txt)
        promise.then(s=>{
            alert("Wellcome back! " + account + " !");
            document.body.style.cursor = "auto";
            cc.director.loadScene("menu");
        })
        .catch(e=>{alert(e); log_in_email_txt = "" ; log_in_password_txt = "";})

        var postsRef = firebase.database().ref("account/" + account);

        this.total_manager.account = account;
            
        postsRef.once('value', (data)=>{

            this.total_manager.fruit = data.val().fruit;
            this.total_manager.skin = data.val().skin;
            this.total_manager.current_skin = data.val().current_skin;
            this.total_manager.stage1_fruit = data.val().stage1_fruit;
            this.total_manager.stage2_fruit = data.val().stage2_fruit;
            this.total_manager.stage3_fruit = data.val().stage3_fruit;
        });

    }
    signup(message){
        cc.audioEngine.playEffect(this.se_click, false);
        var auth = firebase.auth();
        let sign_up_email_txt = this.email.string;
        let sign_up_password_txt = this.password.string;

        var account_txt = this.email_to_account(sign_up_email_txt);

        var postsRef = firebase.database().ref('account/' + account_txt);
            postsRef.once('value').then((snapshot)=>{
                var data = snapshot.val();
                const promise = auth.createUserWithEmailAndPassword(sign_up_email_txt,sign_up_password_txt)
                promise.then(s=>{
                    alert("success");
        
                    var postsRef = firebase.database().ref("account/" + account_txt);
        
                    postsRef.set({
                        email: sign_up_email_txt,
                        
                        
                        //skin : data of which skin the player have (binary code method)
                        //     binary code method : if got #1 , #5 , #8 skin , then skin = 2^(1-1) + 2^(5-1) + 2^(8-1) = 145
                        //current_skin : the number of current skin that the player choose;
                        //stage_fruit : the number of fruit that player eaten
                        skin:1,
                        fruit: 0,
                        current_skin:1,
                        stage1_fruit:0,
                        stage2_fruit:0,
                        stage3_fruit:0


                    });
                },
                e=>{alert(e);})
            })


    }

    email_to_account(email : string){
        let account_arr = [];
        for(let letter of email){
            if(letter != "@")
                account_arr.push(letter);
            else
                break;    
        }
        let account = account_arr.join("");
        console.log(account);
        return account;
    }





    
    auto_login(message){
        cc.audioEngine.playEffect(this.se_click, false);
        var auth = firebase.auth();
        let log_in_email_txt = "123@123.com";
        let log_in_password_txt = "123123";

        var account = this.email_to_account(log_in_email_txt);

        const promise = auth.signInWithEmailAndPassword(log_in_email_txt,log_in_password_txt)
        promise.then(s=>{
            alert("Wellcome back! " + account + " !");
            document.body.style.cursor = "auto";
            cc.director.loadScene("menu");
        })
        .catch(e=>{alert(e); log_in_email_txt = "" ; log_in_password_txt = "";})

        var postsRef = firebase.database().ref("account/" + account);

        this.total_manager.account = account;
            
        postsRef.once('value', (data)=>{

            this.total_manager.fruit = data.val().fruit;
            this.total_manager.skin = data.val().skin;
            this.total_manager.current_skin = data.val().current_skin;
            this.total_manager.stage1_fruit = data.val().stage1_fruit;
            this.total_manager.stage2_fruit = data.val().stage2_fruit;
            this.total_manager.stage3_fruit = data.val().stage3_fruit;
        });

    }
}
