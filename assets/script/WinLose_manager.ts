// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {


    // LIFE-CYCLE CALLBACKS:

    private total_manager : any = null;

    onLoad () {
        this.total_manager = cc.find("Total_manager").getComponent("Total_manager");
    }

    start () {

    }

    restart(){
        if(this.total_manager.stage == 1)
            this.total_manager.goto_stage1()
        else if(this.total_manager.stage == 2)
            this.total_manager.goto_stage2()
        else if(this.total_manager.stage == 3)
            this.total_manager.goto_stage3()
    
    }

    go_to_menu(){
        this.total_manager.goto_menu(); 
    }
    // update (dt) {}
}
