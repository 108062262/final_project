// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class Game_manager extends cc.Component {

    @property({type:cc.Node})
    private  pause_box : cc.Node = null;

    @property({type:cc.Node})
    private  buttons_parent : cc.Node = null;

    @property({type:cc.Node})
    private  player : cc.Node = null;

    @property({type:cc.Node})
    private  fruits : cc.Node[] = [];

    private total_manager  : any = null;

    private stage_fruit : number = 0;

    private UI_frame:any = null;
    private UI_fruit_label:cc.Label = null;
    private UI_cell_label:cc.Label = null;

    private is_pausing : boolean = false;

    onLoad () {
        this.total_manager = cc.find("Total_manager").getComponent("Total_manager");
        
    }

    start () {
        if(this.total_manager.stage == 1)
            this.stage_fruit = this.total_manager.stage1_fruit;
        else if(this.total_manager.stage == 2)
            this.stage_fruit = this.total_manager.stage2_fruit;
        else if(this.total_manager.stage == 3)
            this.stage_fruit = this.total_manager.stage3_fruit;

        this.UI_frame = cc.find("Canvas/UI frame");
        this.UI_cell_label = this.UI_frame.getChildByName("cell label").getComponent(cc.Label);
        this.UI_fruit_label = this.UI_frame.getChildByName("fruit label").getComponent(cc.Label);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
        this.init_game();


    }

    init_game(){
        for(let i = 0 ; i < 32; i++){
            this.fruits[i].getComponent("Fruit").number = i+1;
            if(this.stage_fruit%2){
                this.fruits[i].active = false;
            }
            this.stage_fruit = Math.floor(this.stage_fruit/2);
            console.log("this.stage_fruit : " + this.stage_fruit);
        }
    }

    onKeyDown(event) 
    {
        if(event.keyCode != cc.macro.KEY.escape) return;
        if(!this.is_pausing){
            this.pause_box.setPosition(this.player.position);
            //this.pause_box.position = this.pause_box.position.sub(this.canvas.position);
            this.pause_box.active = true;
            cc.director.pause();
            this.pause_box.rotation = -cc.find("Canvas").getChildByName("Main Camera").getComponent("Camera_control").rotate_angle;
            this.buttons_parent.rotation = 2*cc.find("Canvas").getChildByName("Main Camera").getComponent("Camera_control").rotate_angle ;
            this.is_pausing = true;
        }
        else{
            this.cancel_pause();
        }
    }
    onKeyUp(event)
    {
    }

    cancel_pause(){
        this.pause_box.active = false;
            cc.director.resume();
            this.is_pausing = false;
    }

    back_to_menu(){
        cc.director.resume();
        this.total_manager.goto_menu();
    }

    go_to_win(){
        this.total_manager.goto_win_scene();
    }

    go_to_lose(){
        this.total_manager.goto_lose_scene();
    }


                                       /* UI stuff  */

    // camera_zoom_out_rate should be "this.camera_zoom_out_rate" in player , and UI will expand with camera
    // if camera is shrinking , then use "1/this.camera_zoom_out_rate" with camera instead
    adjust_UI_apprearance(camera_zoom_out_rate){
        this.UI_frame.setPosition(this.UI_frame.x * 1/camera_zoom_out_rate,
                                  this.UI_frame.y * 1/camera_zoom_out_rate);
        this.UI_frame.scaleX *= 1/camera_zoom_out_rate;
        this.UI_frame.scaleY *= 1/camera_zoom_out_rate;
    }
    update_UI_fruit_lable(cur_fruit :number){
        this.UI_fruit_label.string = "x " + String(cur_fruit);
    }
    update_UI_cell_lable(cur_cell :number){
        this.UI_cell_label.string = "x " + String(cur_cell);
    }





    reset_fruit(){
        if(this.total_manager.stage == 1)
            this.total_manager.update_to_firebase("stage1_fruit", 0);
        else if(this.total_manager.stage == 2)
            this.total_manager.update_to_firebase("stage2_fruit", 0);
        else if(this.total_manager.stage == 3)
            this.total_manager.update_to_firebase("stage2_fruit", 0);
        
            alert("seccess! plaz logout and login");
    }

    // update (dt) {}
}
