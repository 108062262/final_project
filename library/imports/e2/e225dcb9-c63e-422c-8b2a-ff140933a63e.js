"use strict";
cc._RF.push(module, 'e225dy5xj5CLIsq/xQJM6Y+', 'Flower');
// script/Flower.ts

Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Flower = /** @class */ (function (_super) {
    __extends(Flower, _super);
    function Flower() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.flower_bullet = null;
        _this.shootDistance = 180;
        _this.shootFrequency = 4;
        _this.anim = null;
        _this.animState_attack = null;
        _this.animState_idle = null;
        _this.isShooting = false;
        _this.idle = false;
        return _this;
    }
    Flower.prototype.onLoad = function () {
        this.anim = this.node.getComponent(cc.Animation);
    };
    Flower.prototype.start = function () {
        this.anim.play("flower_idle");
        this.schedule(function () {
            this.shoot();
        }, this.shootFrequency);
    };
    Flower.prototype.update = function () {
        if (this.isShooting && this.anim.getAnimationState('flower_idle').isPlaying) {
            this.animState_idle = this.anim.stop("flower_idle");
        }
        else if (!this.isShooting && !this.anim.getAnimationState('flower_idle').isPlaying) {
            this.animState_idle = this.anim.play("flower_idle");
        }
    };
    Flower.prototype.shoot = function () {
        this.isShooting = true;
        this.anim.play("flower_attack");
        var flower_bullet;
        this.scheduleOnce(function () {
            flower_bullet = cc.instantiate(this.flower_bullet);
            flower_bullet.parent = this.node;
            flower_bullet.setPosition(0, 0);
            var action;
            action = cc.spawn(cc.sequence(cc.moveBy(0.5, -this.shootDistance, 0), cc.delayTime(0.2), cc.removeSelf()), cc.fadeTo(0.5, 0));
            flower_bullet.runAction(action);
            this.isShooting = false;
        }, 0.8);
    };
    __decorate([
        property(cc.Prefab)
    ], Flower.prototype, "flower_bullet", void 0);
    __decorate([
        property
    ], Flower.prototype, "shootDistance", void 0);
    __decorate([
        property
    ], Flower.prototype, "shootFrequency", void 0);
    Flower = __decorate([
        ccclass
    ], Flower);
    return Flower;
}(cc.Component));
exports.default = Flower;

cc._RF.pop();