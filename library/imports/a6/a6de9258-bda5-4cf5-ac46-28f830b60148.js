"use strict";
cc._RF.push(module, 'a6de9JYvaVM9axGKPgwtgFI', 'Camera_control');
// script/Camera_control.ts

// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
Object.defineProperty(exports, "__esModule", { value: true });
var _a = cc._decorator, ccclass = _a.ccclass, property = _a.property;
var Camera_control = /** @class */ (function (_super) {
    __extends(Camera_control, _super);
    function Camera_control() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.player_node = null;
        _this.max_rotate_angle = 60;
        // last percentage of rotation will be slow to stop
        _this.slow_rotate_percentage = 1 / 5;
        _this.map = null;
        _this.canvas = null;
        _this.camera = null;
        _this.rotate_angle = 0;
        _this.rightDown = false; // key for player to jump
        _this.leftDown = false; // key for player to jump
        return _this;
    }
    // LIFE-CYCLE CALLBACKS:
    Camera_control.prototype.onLoad = function () {
        cc.director.getPhysicsManager().enabled = true;
    };
    Camera_control.prototype.start = function () {
        this.camera = this.node.parent;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    };
    Camera_control.prototype.update = function (dt) {
        //camera follow player
        var player_pos = this.player_node.getPosition();
        this.camera.setPosition(player_pos);
        //rotate comara
        if (this.leftDown) {
            if (this.camera.angle >= -this.max_rotate_angle) {
                if (this.camera.angle <= -this.max_rotate_angle * (1 - this.slow_rotate_percentage)) { //last part of rotation be slow
                    this.camera.angle -= (this.max_rotate_angle + this.camera.angle) / (this.max_rotate_angle * this.slow_rotate_percentage);
                }
                else
                    this.camera.angle -= 1;
            }
        }
        else if (this.rightDown) {
            if (this.camera.angle <= this.max_rotate_angle) {
                if (this.camera.angle >= this.max_rotate_angle * (1 - this.slow_rotate_percentage)) { //last part of rotation be slow
                    this.camera.angle += (this.max_rotate_angle - this.camera.angle) / (this.max_rotate_angle * this.slow_rotate_percentage);
                }
                else
                    this.camera.angle += 1;
            }
        }
        this.rotate_angle = this.camera.angle;
        //rotate gravity
        cc.director.getPhysicsManager().gravity = cc.v2(-100 * Math.cos((this.camera.angle + 90) * 2 * Math.PI / 360), 100 * Math.sin((this.camera.angle - 90) * 2 * Math.PI / 360));
    };
    Camera_control.prototype.onKeyDown = function (event) {
        switch (event.keyCode) {
            case cc.macro.KEY.right:
                console.log(cc.macro.KEY.right);
                this.rightDown = true;
                break;
            case cc.macro.KEY.left:
                this.leftDown = true;
                break;
        }
    };
    Camera_control.prototype.onKeyUp = function (event) {
        switch (event.keyCode) {
            case cc.macro.KEY.right:
                this.rightDown = false;
                break;
            case cc.macro.KEY.left:
                this.leftDown = false;
                break;
        }
    };
    __decorate([
        property(cc.Node)
    ], Camera_control.prototype, "player_node", void 0);
    __decorate([
        property
    ], Camera_control.prototype, "max_rotate_angle", void 0);
    __decorate([
        property
    ], Camera_control.prototype, "slow_rotate_percentage", void 0);
    Camera_control = __decorate([
        ccclass
    ], Camera_control);
    return Camera_control;
}(cc.Component));
exports.default = Camera_control;

cc._RF.pop();